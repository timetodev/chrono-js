/* the function getElementByTagName returns a list of elements with the tag name given here "span". */
var sp = document.getElementsByTagName("span");// span qui affiche les valeur du chrono
var btn_start=document.getElementById("start"); // Button Start du Chrono
var btn_stop=document.getElementById("stop"); // Button stop du chrono
var btn_save=document.getElementById("save"); // button REC du chrono
var ms=0,s=0,mn=0,h=0; // valeur initial du chrono non start
var t;
var list=document.getElementById("list");

document.querySelector("#start").addEventListener("click", start);
document.querySelector("#stop").addEventListener("click", stop);
document.querySelector("#reset").addEventListener("click", reset);
document.querySelector("#save").addEventListener("click", save);

// Function du Var Ligne 3 pour mettre en marche le chrono
// Function of the Var Line 3 to start the chrono
function start(){
 t =setInterval(update_chrono,100);
 btn_start.disabled=true;
}

// Function qui update le chrono
function update_chrono(){
  ms+=1;
  
     if(ms==10){
      ms=1;
      s+=1;
     }
     
     if(s==60){
      s=0;
      mn+=1;
     }
     if(mn==60){
      mn=0;
      h+=1;
     }
    
     sp[0].innerHTML=h+" h";
     sp[1].innerHTML=mn+" min";
     sp[2].innerHTML=s+" s";
     sp[3].innerHTML=ms+" ms";
}

// function stop the chrono
// functio qui stop le chrono
  function stop(){
  clearInterval(t);
  btn_start.disabled=false;

}

// function reset the chrono
// function qui reset le chrono
function reset(){
 clearInterval(t);
  btn_start.disabled=false;
  ms=0,s=0,mn=0,h=0;


  
  sp[0].innerHTML=h+" h";
  sp[1].innerHTML=mn+" min";
  sp[2].innerHTML=s+" s";
  sp[3].innerHTML=ms+" ms";

  console.log(list) ;
  document.querySelector("#list").innerHTML = "" ;

}

// function the save current value the chrono
// function qui sauvegarde les valeurs actuel du chrono
function save() {
    list.innerHTML += "<div class=\"alert alert-dismissible alert-danger\">" + h + " h : " + mn + " mn : " + s + " s : " + ms + " ms " + "</div>" + "<br><br>";
}



   
  
